# Achievement Runner
## Initial Project Overview
- User can create their own quests with generalized expected time commitments (say short, moderate, long)
- System will asign a score to each quest on creation (randomized in range based on time commitment)
- Quest screen will allow user to turn in the quest, at which point they receive a nice animation and sound to celebrate the completion
- Score is added to lifetime quest score
- Main screen shows quests
- Quests should have a title, description, and optional icon
- Data is stored in the app. Possibly a Google Drive backup option
## Possible Extensions
- API for integrations with other products. For instance, a hackathon could use the API to have curated quests for users
- Curated quests that can be verified by the quest giver could have leaderboards. Leaderboards would always be per integration, no global
- Multi-step quests
- Recurring quests (i.e. "read an entire book this month")
- An easy way to find curated quests (community and integration options)
- Should experiment with various secure ways to store user data either where they own the storage or encrypted on servers where only they have the key
- Friends list to optionally share your own quests with others
- Multiplayer quests
- Location based quests